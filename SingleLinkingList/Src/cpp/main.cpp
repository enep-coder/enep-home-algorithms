#include <iostream>
#include <string>


using namespace std;
struct listitem {
    string item;
    listitem *Plink;
};

class SingleLinkedList {
private:
    listitem *first;
    listitem *last;
    listitem *head;

    void setFirstItem(listitem *l) {
        this->first = l;
        this->head = l;
        this->last = l;
    }
public:
    SingleLinkedList() {
        this->first=NULL;
        this->last=NULL;
        this->head=NULL;
    }
    void PrintList() {
        int c=1;
        this->head = this->first;
        cout<<"List single linking list"<<endl;
        do{
            cout<<" "<<c<<" - "<<this->head->item<<endl;
            c++;
            this->head = this->head->Plink;
        } while(this->head != NULL);
        cout<<"Capacity: "<<c-1<<endl;
    }
    void AddItemAfter(string s) {
        listitem *i = new listitem;

        i->item = s;
        i->Plink=NULL;

        i->Plink = this->first;
        this->first = i;

        return;

    }
    void addItemBefore(string s) {
        listitem *i = new listitem;
        i->item = s;
        i->Plink=NULL;
        if(this->first == NULL) {
            this->setFirstItem(i);
        } else {
            this->last->Plink = i;
            this->last = i;
            cout<<"Last: "<<this->last->item<<endl;
        }
        return;
    }
};

int main(int argc, char **argv)
{

    SingleLinkedList *lst = new SingleLinkedList;

    cout << "Workling single linking list" <<endl;
     cout << "----------------------------" <<endl;
     cout<<"Add 3 items into before"<<endl;

      lst->addItemBefore("test1 before");
      lst->addItemBefore("test2 before");
      lst->addItemBefore("test3 before");

    cout<<"Add 3 items into after"<<endl;

    lst->AddItemAfter("test4 after");
    lst->AddItemAfter("test5 after");
    lst->AddItemAfter("test6 after");
    cout<<endl;
    lst->PrintList();

    return 0;
}
