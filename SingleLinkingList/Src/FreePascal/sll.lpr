program sll;

{$mode objfpc}{$H+}

uses Classes, SysUtils;

type
  pListItem = ^rListItem;
  rListItem = record
    item:string;
    Plink:pListItem;
  end;
type

  { SinglelLinkingList }

  SinglelLinkingList = class
    public
      first:PListItem;
      last:PListItem;
      head:PListItem;
      constructor Create;
      destructor destroy;
      procedure AddItemAfter(s:string);
      procedure AddItemBefore(s:string);
      procedure DeleteItemAfer;
      procedure DeleteItemBefore;
      procedure PrintList;
  end;




{ SinglelLinkingList }

constructor SinglelLinkingList.Create;
begin
  self.first:=nil;
  self.last:=nil;
  self.head:=nil;
end;

destructor SinglelLinkingList.destroy;
begin

end;

procedure SinglelLinkingList.AddItemAfter(s: string);
var
  i:PListItem;
begin
  new(i);
  i^.item := s;
  i^.Plink := nil;
  if first = nil then
  begin
    self.first:=i;
    self.last:=i;
    self.head:=i;
  end
  else
  begin
    i^.Plink := self.first;
    self.first := i;
    self.head:=i;

  end;

end;

procedure SinglelLinkingList.AddItemBefore(s: string);
var
  i:PListItem;
begin
  new(i);
  i^.item := s;
  i^.Plink := nil;
  if first = nil then
  begin
    self.first:=i;
    self.last:=i;
    self.head:=i;

  end
  else
  begin
    self.last^.Plink:=i;
    self.last:=i;


  end;
end;

procedure SinglelLinkingList.DeleteItemAfer;
var
  t:PListItem;
begin
   if self.first <> nil then
   begin
       t:=self.first;
       self.first:=self.first^.Plink;
       dispose(t)
   end
   else
   begin
     self.first:=nil;
     self.last:=nil;
     self.head:=nil;
   end;

end;

procedure SinglelLinkingList.DeleteItemBefore;
var l,d:PListItem;
begin
  if self.last <> nil then
  begin
    l:=self.first;
    while l^.Plink <> nil do
    begin
         d:=l;
         l:=l^.Plink;
         write('.');
    end;
    d^.Plink:=nil;
    dispose(l);

  end;
end;

procedure SinglelLinkingList.PrintList;
var
  c:integer;
begin
  writeLn('List items by Single linking list');
  c:=0;
  self.head := self.first;
  while self.head <> nil do
  begin
       writeln(inttostr(c)+' - '+self.head^.item);
       inc(c);
       self.head:=self.head^.Plink;
  end;
  writeln('Capacity: '+ inttostr(c));
end;

var
  list: SinglelLinkingList;

begin

  list:= SinglelLinkingList.Create;
  writeln('Workling single linking list');
  writeln('----------------------------');
  writeln('Add 3 items into before');
  list.AddItemBefore('test1 before');
  list.AddItemBefore('test2 before');
  list.AddItemBefore('test3 before');
  writeln('Add 3 items into after');
  list.AddItemafter('test4 after');
  list.AddItemAfter('test5 after');
  list.AddItemAfter('test6 after');

  list.PrintList;
   list.DeleteItemAfer;
   list.PrintList;
   writeln('Delete before item');
   list.DeleteItemBefore;
   writeln('Delete before item');
   list.DeleteItemBefore;
   writeln('Delete before item');
   list.DeleteItemBefore;
  list.PrintList;
    writeln('----------------Delete 1 item by after ----------------');
   list.DeleteItemAfer;
   list.PrintList;
  readln;
end.

